<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>All Daftar</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>b1c0a70a-05dd-44b8-9e80-b1b13993d6f7</testSuiteGuid>
   <testCaseLink>
      <guid>77b55c94-d4e7-47b9-82b1-c2f82dcfede8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Daftar/Daftar_1 - Success Register</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a58db6f3-df25-4f03-9e5e-fee6b95a0684</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Daftar/Daftar_2 - BUG-Error Mohon Maaf</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c305461f-900f-4eaf-9d52-b873beea92d2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Daftar/Daftar_3 - Email sudah digunakan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ca997d7a-bdea-449c-a40a-b05f34409220</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Daftar/Daftar_4 - Nama_Bagian ini harus diisi</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d93129cb-9527-4477-a1af-32ffc5731d6d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Daftar/Daftar_5 - Email_Bagian ini harus diisi</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f35bf631-441d-4806-9ad9-e7cd7e0d6cc6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Daftar/Daftar_6 - Password_Bagian ini harus diisi</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7f1fe4fd-6da9-49f4-9357-ee9fcbee67a1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Daftar/Daftar_7 - Phone_Bagian ini harus diisi</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1b28c5b2-c62c-4cd0-b4e7-9fcd8a360349</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Daftar/Daftar_8 - JenisAkun_Bagian ini harus diisi</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
